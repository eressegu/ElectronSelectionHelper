#include "ElectronSelectionHelper/ElectronSelectionHelper.h"

// checks the kinematics of the object
bool ElectronSelectionHelper::isElectronGood(const xAOD::Electron* electron) {

  bool passSelect(false);

  // need to veto the crack 
  if ( electron->pt() > 10000 && fabs(electron->eta()) < 2.47 && (fabs(electron->eta()) < 1.37 || fabs(electron->eta()) > 1.52) ){
    passSelect = true;
  }

  return passSelect;
}

